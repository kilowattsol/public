# open aliases
alias vi_alias='vi ~/.bash/.bash_aliases'
alias bash_update='source ~/.bashrc'

# python default version
alias python='python3'
alias pip='pip3'
